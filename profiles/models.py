from django.db import models
# Create your models here.
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.contrib import admin

class UserProfile(models.Model):
    """ This model extends user the django user model In this case, am adding 
    Phone Number. we will be adding Area as we said later.
    This is just for demonstration"""
    AREA_CHOICES = (('Nairobi', 'Nairobi'),('Mombasa', 'Mombasa'),)

    phone_number = models.CharField(max_length=15, null=True, blank=True)
    area = models.CharField(max_length=30, null=True, blank=True,choices=AREA_CHOICES)
    user = models.ForeignKey(User, unique=True)
    def __unicode__(self):
        return unicode(self.user)
   

def create_user_profile(sender, **kwargs):
    """When creating a new user, make a profile for him or her."""
    u = kwargs["instance"]
    if not UserProfile.objects.filter(user=u):
        UserProfile(user=u).save()

post_save.connect(create_user_profile, sender=User) #fire the signal


