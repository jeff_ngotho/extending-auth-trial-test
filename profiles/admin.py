from django.contrib import admin
from .models import UserProfile
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'phone_number','area' )
    list_filter = ('phone_number','user')
    raw_id_fields = ('user',)


admin.site.register(UserProfile, ProfileAdmin)