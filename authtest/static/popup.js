jQuery(function(){
	//The popup html.
	var popup_html='<div class="wsm-overlay"></div>'
				+'<div class="wsm-popup-container">'
				+'<div class="wsm-popup-close">'
				+'<a href="javascript:void(0)" >x</a></div>'
				+'<div class="wsm-popup-title"></div>'
				+'<div class="wsm-popup-content"></div>'
				+'</div>';
	//Append the popup html
	jQuery('body').append(popup_html);
	
	// close popup.
	jQuery('body').on('click','.wsm-popup-container .wsm-popup-close a',function(){
		jQuery('.wsm-popup-container').fadeOut(1000);
		jQuery('.wsm-overlay').fadeOut(1000);
	});
	
	
	
});
// Show the popup
	function wsmShowPopUp(title,content,show_close)
	{
		jQuery('body .wsm-popup-container .wsm-popup-title').html(title);
		jQuery('body .wsm-popup-container .wsm-popup-content').html(content);
		jQuery('body .wsm-overlay').fadeIn(1000);
		jQuery('body .wsm-popup-container').fadeIn(1000);
		if(show_close==false)
		{
			jQuery('body .wsm-popup-container .wsm-popup-close').hide();
		}
		else
		{
			jQuery('body .wsm-popup-container .wsm-popup-close').show();
		}
		
	}
